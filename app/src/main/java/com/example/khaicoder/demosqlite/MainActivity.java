package com.example.khaicoder.demosqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.khaicoder.demosqlite.adapter.AdapterContacts;
import com.example.khaicoder.demosqlite.precenter.DoContacts;

public class MainActivity extends AppCompatActivity {
    private EditText edtName, edtPhone;
    private RecyclerView recyclerView;
    private DoContacts doContacts;
    private static final String TAG = MainActivity.class.getSimpleName();
    private AdapterContacts contacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        doContacts = new DoContacts(this);

        initsRecycleView();
        initsView();


    }

    private void initsRecycleView() {
        recyclerView = findViewById(R.id.rc_view);
        contacts = new AdapterContacts(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(contacts);
    }

    private void initsView() {
        edtName = findViewById(R.id.edt_name);
        edtPhone = findViewById(R.id.edt_phone);

        findViewById(R.id.btn_insert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doContacts.insert(edtName.getText().toString(), edtPhone.getText().toString());
                Toast.makeText(MainActivity.this, "insert succes", Toast.LENGTH_SHORT).show();
                contacts.notifyDataSetChanged();
            }
        });

        findViewById(R.id.btn_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doContacts.delelte(edtName.getText().toString());
                contacts.notifyDataSetChanged();
            }
        });

        findViewById(R.id.btn_update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doContacts.update(edtName.getText().toString(), edtPhone.getText().toString());
                contacts.notifyDataSetChanged();
            }
        });


    }


}
