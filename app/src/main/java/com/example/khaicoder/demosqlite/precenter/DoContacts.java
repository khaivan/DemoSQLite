package com.example.khaicoder.demosqlite.precenter;

import android.content.Context;

import com.example.khaicoder.demosqlite.adapter.AdapterContacts;
import com.example.khaicoder.demosqlite.model.Contacts;
import com.example.khaicoder.demosqlite.model.DBHelper;

import java.util.List;

public class DoContacts implements IDoContact {
    private DBHelper dbHelper;
    private AdapterContacts adapterContacts;

    public DoContacts(Context context) {
        dbHelper = new DBHelper(context);

    }


    @Override
    public void insert(String name, String phone) {
        dbHelper.insert(name, phone);
    }

    @Override
    public void update(String name, String phone) {
        dbHelper.update(name, phone);
    }

    @Override
    public void delelte(String name) {
        dbHelper.delelte(name);
    }

    @Override
    public List<Contacts> getAll() {
        return dbHelper.getAll();
    }


}
