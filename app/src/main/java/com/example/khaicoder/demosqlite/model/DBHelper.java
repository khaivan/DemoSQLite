package com.example.khaicoder.demosqlite.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {
    private static String DATA_NAME = "mydb";
    private static String NAME = "name";
    private static String PHONE = "phone";
    private List<Contacts> contacts;

    public DBHelper(Context context) {
        super(context, DATA_NAME, null, 1);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + DATA_NAME + "(name String primary key,phone String)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table  if exists " + DATA_NAME);
        onCreate(db);

    }


    public void insert(String name, String phone) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NAME, name);
        values.put(PHONE, phone);
        db.insert(DATA_NAME, null, values);

    }


    public void update(String name, String phone) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NAME, name);
        values.put(PHONE, phone);
        db.update(DATA_NAME, values,"name=?",new String[]{name});
    }


    public void delelte(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DATA_NAME, "name=?", new String[]{name});

    }


    public List<Contacts> getAll() {
        contacts = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select*from " + DATA_NAME, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String name = cursor.getString(cursor.getColumnIndex(NAME));
            Log.d("DBHelper", "name: " + name);
            String phone = cursor.getString(cursor.getColumnIndex(PHONE));
            Log.d("DBHelper", "phone: " + phone);

            contacts.add(new Contacts(name, phone));
            cursor.moveToNext();
        }

        return contacts;
    }
}
