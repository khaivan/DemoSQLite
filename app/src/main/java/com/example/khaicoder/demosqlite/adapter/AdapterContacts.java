package com.example.khaicoder.demosqlite.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.khaicoder.demosqlite.R;
import com.example.khaicoder.demosqlite.model.Contacts;
import com.example.khaicoder.demosqlite.precenter.DoContacts;

public class AdapterContacts extends RecyclerView.Adapter<AdapterContacts.Viewholder> {
    private DoContacts doContacts;

    public AdapterContacts(Context context) {
        doContacts = new DoContacts(context);
    }

    @NonNull
    @Override
    public AdapterContacts.Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item, viewGroup, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterContacts.Viewholder viewholder, int position) {
        Contacts contacts = doContacts.getAll().get(position);
        viewholder.tvName.setText(contacts.getName());
        viewholder.tvPhone.setText(contacts.getPhone());
    }

    @Override
    public int getItemCount() {
        return doContacts.getAll().size();
    }

    class Viewholder extends RecyclerView.ViewHolder {
        private TextView tvName, tvPhone;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPhone = itemView.findViewById(R.id.tv_phone);
        }
    }

}
