package com.example.khaicoder.demosqlite.precenter;

import com.example.khaicoder.demosqlite.model.Contacts;

import java.util.List;

public interface IDoContact {
    void insert(String name,String phone);
    void update(String name,String phone);
    void delelte(String name);
    List<Contacts> getAll();

}
